import Employee from '../employee/Employee';
import './Employees.css'

export default function Employees() {
    let employees = [
        { name: 'vipin', age: 40, place: 'fbd' },
        { name: 'vishnu', age: 30, place: 'noida' },
    ]
    return (

        <div className="main-employees">
            {employees.map((emp) => (
                <Employee key={emp.age} employee={emp} />
                ))}
        </div>        
    );
}